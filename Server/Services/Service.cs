using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace Server
{
    public class Service : StarSign.StarSignBase
    {
        private readonly ILogger<Service> _logger;
        private readonly string[] _starSignFileContent;

        public Service(ILogger<Service> logger)
        {
            _logger = logger;
            _starSignFileContent = System.IO.File.ReadAllLines(@"..\Server\StarSign.txt");
        }
        
        public String GetStarSign(string dateString, string[] lines)
        {
            DateTime date = DateTime.Parse(dateString);

            date = new DateTime(DateTime.Now.Year, date.Month, date.Day);

            string[] vect;
            DateTime date1;
            DateTime date2;
            String starSign;

            foreach (string line in lines)
            {
                vect = line.Split(' ');
                date1 = DateTime.Parse(vect[0]);                             
                date2 = DateTime.Parse(vect[1]);
                starSign = vect[2];
                              
                if (date.CompareTo(date1) >= 0 && date.CompareTo(date2) < 0 || 
                    date.CompareTo(date1) > 0 && date.CompareTo(date2) <= 0)
                {
                    return starSign;
                }
            }

            return "Capricorn"; 
        }

        public override Task<StarSignReply> GetStarSign(DateRequest request, ServerCallContext context)
        {
            return Task.FromResult(new StarSignReply
            {
                StarSign = GetStarSign(request.Date,_starSignFileContent)
            });
        }
    }
}
