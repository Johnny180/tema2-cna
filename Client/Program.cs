﻿using Grpc.Net.Client;
using System;
using System.Globalization;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        static async Task Main()
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Server.StarSign.StarSignClient(channel);

            Console.WriteLine("Please enter your birthday: ");

            string date = Console.ReadLine();

            bool valid = DateTime.TryParseExact(date, "M/d/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime parsed);

            if (!valid)
            {
                Console.WriteLine("\n ERROR: Invalid input!\n");
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
                return;
            }

            var reply = await client.GetStarSignAsync(
                              new Server.DateRequest { Date = date });

            Console.WriteLine($"\nYour Star Sign is : {reply.StarSign}!\n");

            channel.ShutdownAsync().Wait();

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
